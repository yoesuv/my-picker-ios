//
//  My_PickerApp.swift
//  My Picker
//
//  Created by Yusuf Saifudin on 02/03/22.
//

import SwiftUI

@main
struct My_PickerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
